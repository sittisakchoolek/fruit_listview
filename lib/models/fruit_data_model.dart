// Name: Sittisak Choolak
// Student ID: 6450110013

class FruitDataModel {
  final String name, imageUrl, desc;
  // constructor
  FruitDataModel(this.name, this.imageUrl, this.desc);
}
